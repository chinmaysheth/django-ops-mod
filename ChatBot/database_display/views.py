import pyrebase
import firebase_admin
import json 
import asyncio
import datetime
from datetime import *
from django.db.models import Max,Count
from django.template.loader import render_to_string
from pytz import timezone
from django.shortcuts import render
from django.db import connection
from truebil_dsm.auction_app.models import DealerListingBid,ListingAuction,Dealer,AuctionStatus,Wallet
from truebil_dsm.user.models import User,City
from truebil_dsm.main.models import Listing,Variant
from django.http import HttpResponse
from firebase import firebase
from firebase.firebase import FirebaseApplication
from firebase_admin import credentials,auth,db
from channels.generic.http import AsyncHttpConsumer

# Date Format
dt_format = "%d/%m/%Y %H:%M"
zone=timezone('Asia/Kolkata')
now=datetime.now()
# Firebase
config = {
    'apiKey': 'AIzaSyBk-7IZSw-IJd5ag5ygGUsZGtXsu_uSihc',
    'authDomain': 'truebil-auction-app.firebaseapp.com',
    'databaseURL': 'https://truebil-auction-app.firebaseio.com',
    'projectId': 'truebil-auction-app',
    'storageBucket': 'truebil-auction-app.appspot.com',
    'messagingSenderId': '977779327744',
    'appId': '1:977779327744:web:d073034294e7f5373629e5'
  };


adminConfig={
    "type": "service_account",
  "project_id": "truebil-auction-app",
  "private_key_id": "10910d8ff19ad02051e483d70b9007bd304d0799",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDW4Ewr5Z9BphBf\nVYUtj0zfj4e/VJcu9gDKOGWfq1cuS/Bqd1PO8s5IckWhShHdGsR6E1dYnnggWFT2\ns1YL91RaKfQ9Zd37GRFqdxkmdSd8AqGUOtOdI3wz4JlamwGktg00rnHPvCecb4ky\n4xRF0L1+1qjrinxHGwSbtErmmwq61pgNByy2pZLoaOBONGwV7oarXJFJp06R6aGx\n865u9yFg7oOZVBNpFag7lvM5WyU+/R1t0t9LJOawwNkxY2xg4rHokuDjnhO5tFgw\nBbk8x/FVC1jZnvsBMPFVrDDqaMYwGIaspd3MExw8fH8BaW+nbPBcuv/ImIu7NYUz\np94NN0LvAgMBAAECggEAErWmn3V3wtuWz8MLVmA34IkHpN07HL4TnrsyZHWJOpRa\nJqsLnRiYy/rg0Gs518RQ5a0LDShSa8T4Z1K0l1SKmVJpD3K81VqyOlCMdapik1jk\nGmgIE19xNrZdVzC2ZlHqH6aUByaDvHDpw7u9NCcGZb9P9GwAGwdIBQns/twl69N1\n9Fym/GF7xIEDAR/LK/nozjqpeR+GSEFfVyBk75ynTuEA05tiOIW/tM9g6jazwYDe\nQ085CgwfzIuSYkjPhbsQR2OK8UgIcxnEdmie9laMT1WZDZZSIgxWwE5d22ZMWieX\nXq3bATmPSrTsjnvufUjdGKnFitpfPFsmAMDnH6Cv4QKBgQDveZBm+LnCGEbWkA6n\n1z6fiVPDCfYC7SqsxJzwiw8yjQ6nUiHF4uEdsB8fAHJtHW8GF5wsOhwGyumNt25/\naCGEy0JSgT/hA+MpqlniWfrtcOiW+GcASmWJihgGaZT68Dn7f2xzT6+A4grIFHYo\nto4Ra2L3yS+kKH9KeTW6RigaLQKBgQDltC+YzDhTU2rzpYubl6UHdpP/pHYj7Hf8\nQxdAGOEgYEvUUbPjwxNC5vSlrIbCmgIYun5/sAJLtz+seWssIYRKCgOcs/2mPdi8\n9bSkggLEPxDnIj4hBwpR36mjergZLmTGHPJ+luLZEkK7eWZnJqmBBexoQuLMpCek\nZDduFLuPCwKBgQCUOeV0px2rkwU9x7sajnN3ojPTdHXux8fzWltd1GUawA7C6GZ5\nKv52Ns58jPVGrsJKvhBvm6Q/XBeGHXTqrZSZsoohsVjNuW9BGe062sEjeRCuaaCv\noF0K7gwC5BVP4aOyFOQW18rk9oT/5RJiDuUOyS5f+wpfDKYFfdMQsl4eHQKBgFRp\nxyQLyJc2jJNFPuzK6aa4eOL1eUTyXZWiwiSYAAN53+QCsmz8/1MStM09fbh+CE82\nGc6qy+0vRoMHi88H8imTTIiFzK0PBVtLoDrkXEfqNTf7aYGRqMU1wI2HR6hZWEdF\n1AeZQ3wqLcPfdTZrCteTabFXAynZGYhcG9KkY9jJAoGAfL0g/YB+1EdU/4R660mt\nOn00P/WmOcJ47g63aZ8qUBYNw+r44rTKUIizFzFxGohia3tYTJAbDu25lCF/aD32\nohx0035sYeD9H3mLqEho73GI3+1zxm4mcsqrM/LOh6zNhwVPTdOargLjeqetnCGI\nTP1E+XY7IXl/Yx+qQboUmRo=\n-----END PRIVATE KEY-----\n",
  "client_email": "firebase-adminsdk-2mz4m@truebil-auction-app.iam.gserviceaccount.com",
  "client_id": "112307189845918609805",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-2mz4m%40truebil-auction-app.iam.gserviceaccount.com"    
}

def initialize_firebase():
    try:
        firebase_auction_app = firebase_admin.get_app(name='auction_app')
    except ValueError:
        cred = firebase_admin.credentials.Certificate(adminConfig)
        firebase_auction_app = firebase_admin.initialize_app(cred, {
            'databaseURL': 'https://truebil-auction-app.firebaseio.com'
        }, name='auction_app')
    return firebase_auction_app

fa = initialize_firebase()
firebase_db = db.reference('/', fa)
d1 = firebase_db.child('BidInfo/1').get()


# Create your views here.
def index(request):
    return render(request,'index.html',{})

def db(request):
    
    # Date Format
    dt_format = "%d/%m/%Y %H:%M"
    zone=timezone('Asia/Kolkata')

    response = []
    bids = DealerListingBid.objects.values('listing_auction_id').annotate(count=Max('id')).values_list('count',flat=True).filter(listing_auction__auction_status__id=1,dealer__user__city__name='Mumbai')
    dealer_listing_data = DealerListingBid.objects.filter(id__in=bids).values('listing_auction_id','listing_auction__listing__id','listing_auction__listing__variant__name','dealer__user__name','dealer__user__mobile','dealer__user__city__name','amount','created_at','listing_auction__start_time','listing_auction__end_time','auto_bid_amount','listing_auction__listing__registration_number').order_by('-created_at')
   

    print(dealer_listing_data)
    dealer_listing_data = list(dealer_listing_data)
    for d in dealer_listing_data:
        res = {}
        
        res['Auction_id']=d['listing_auction_id']
        res['CarId']=d['listing_auction__listing__id']
        res['Car']=d['listing_auction__listing__variant__name']
        res['Dealer_name']=d['dealer__user__name']
        res['number']=d['dealer__user__mobile']
        res['City']=d['dealer__user__city__name']
        res['currentAmount']=d['amount']
        res['created_at']=d['created_at'].astimezone(zone).strftime(dt_format)
        res['auto_bid_amount']=d['auto_bid_amount']
        res['bid_start']=d['listing_auction__start_time'].astimezone(zone).strftime(dt_format)
        res['bid_end']=d['listing_auction__end_time'].astimezone(zone).strftime(dt_format)
        res['registration_number']=d['listing_auction__listing__registration_number']
        response.append(res)

        text ='Latest Dealer Bids'
    
    context= {
        'response':response,
        'text': text,
        }

    html = render_to_string('data.html', context)
    return HttpResponse(html)

def dataFromDB(request):

    # Date Format
    dt_format = "%d/%m/%Y %H:%M"
    zone=timezone('Asia/Kolkata')

    response = []
    bids = DealerListingBid.objects.values('listing_auction_id').annotate(count=Max('id')).values_list('count',flat=True).filter(listing_auction__auction_status__id=1,dealer__user__city__name='Mumbai')
    dealer_listing_data = DealerListingBid.objects.filter(id__in=bids).values('listing_auction_id','listing_auction__listing__id','listing_auction__listing__variant__name','dealer__user__name','dealer__user__mobile','dealer__user__city__name','amount','created_at','listing_auction__start_time','listing_auction__end_time','auto_bid_amount','listing_auction__listing__registration_number').order_by('-created_at')
   

    print(dealer_listing_data)
    dealer_listing_data = list(dealer_listing_data)
    for d in dealer_listing_data:
        res = {}
        
        res['Auction_id']=d['listing_auction_id']
        res['CarId']=d['listing_auction__listing__id']
        res['Car']=d['listing_auction__listing__variant__name']
        res['Dealer_name']=d['dealer__user__name']
        res['number']=d['dealer__user__mobile']
        res['City']=d['dealer__user__city__name']
        res['currentAmount']=d['amount']
        res['created_at']=d['created_at'].astimezone(zone).strftime(dt_format)
        res['auto_bid_amount']=d['auto_bid_amount']
        res['bid_start']=d['listing_auction__start_time'].astimezone(zone).strftime(dt_format)
        res['bid_end']=d['listing_auction__end_time'].astimezone(zone).strftime(dt_format)
        res['registration_number']=d['listing_auction__listing__registration_number']
        response.append(res)

        text ='Latest Dealer Bids'
    
    context= {
        'response':response,
        'text': text,
        }
        
    return render(request,'index.html',context)

def dataFromDBcity(request,city_name):
  
    # City
    if city_name in [None, '']:
        city_name = 'Mumbai'
    
    # Date Format
    dt_format = "%d/%m/%Y %H:%M"
    zone=timezone('Asia/Kolkata')

    response = []
    bids = DealerListingBid.objects.values('listing_auction_id').annotate(count=Max('id')).values_list('count',flat=True).filter(listing_auction__auction_status__id=1,dealer__user__city__name=city_name)
    dealer_listing_data = DealerListingBid.objects.filter(id__in=bids).values('listing_auction_id','listing_auction__listing__id','listing_auction__listing__variant__name','dealer__user__name','dealer__user__mobile','dealer__user__city__name','amount','created_at','listing_auction__start_time','listing_auction__end_time','auto_bid_amount','listing_auction__listing__registration_number').order_by('-created_at')
   

    print(dealer_listing_data)
    dealer_listing_data = list(dealer_listing_data)
    for d in dealer_listing_data:
        res = {}
        
        res['Auction_id']=d['listing_auction_id']
        res['CarId']=d['listing_auction__listing__id']
        res['Car']=d['listing_auction__listing__variant__name']
        res['Dealer_name']=d['dealer__user__name']
        res['number']=d['dealer__user__mobile']
        res['City']=d['dealer__user__city__name']
        res['currentAmount']=d['amount']
        res['created_at']=d['created_at'].astimezone(zone).strftime(dt_format)
        res['auto_bid_amount']=d['auto_bid_amount']
        res['bid_start']=d['listing_auction__start_time'].astimezone(zone).strftime(dt_format)
        res['bid_end']=d['listing_auction__end_time'].astimezone(zone).strftime(dt_format)
        res['registration_number']=d['listing_auction__listing__registration_number']
        response.append(res)

    text ='Latest Dealer Bids'
    
    context= {
        'response':response,
        'text': text,
        }
        
    # return render(request,'index.html',context)
    html = render_to_string('data.html', context)
    return HttpResponse(html)

def dataFromFB(request):

    objects = DealerListingBid.objects.all()
    res= objects.raw('Select * from dealer_listing_bids order by id desc limit 5')
    fb = pyrebase.initialize_app(config)
    database=fb.database()
    d=database.child('BidInfo').child('18203').get().val()
    #print(d)
    #ref
    AuctionRef = firebase_db.child('BidInfo/18203/highest_auto_bid/auto_bid_amount').get()
    # print (firebase_db.get())
    context={
        'res':res,
        'd':d,
        'AuctionRef':AuctionRef,
        }
    return render(request,'fbindex.html',context)

def closed(request):
    response = []
    path_info = request.META.get('PATH_INFO')
    print(path_info)
    k='scheduled'
    if not k in path_info:
        bids = DealerListingBid.objects.filter(listing_auction__end_time__lt=now).values('listing_auction_id').annotate(count=Max('id')).values_list('count',flat=True)
        dealer_listing_data=DealerListingBid.objects.filter(id__in=bids).values('listing_auction_id','listing_auction__listing__id','listing_auction__listing__variant__name','dealer__user__name','dealer__user__mobile','dealer__user__city__name','amount','created_at','listing_auction__start_time','listing_auction__end_time','auto_bid_amount').order_by('-created_at')   
    else:
        bids = DealerListingBid.objects.filter(listing_auction__start_time__gt=now).values('listing_auction_id').annotate(count=Max('id')).values_list('count',flat=True)
        dealer_listing_data=DealerListingBid.objects.filter(id__in=bids).values('listing_auction_id','listing_auction__listing__id','listing_auction__listing__variant__name','dealer__user__name','dealer__user__mobile','dealer__user__city__name','amount','created_at','listing_auction__start_time','listing_auction__end_time','auto_bid_amount')    
   
    for d in dealer_listing_data:
        res = {}
        res['Auction_id']=d['listing_auction_id']
        res['CarId']=d['listing_auction__listing__id']
        res['Car']=d['listing_auction__listing__variant__name']
        res['Dealer_name']=d['dealer__user__name']
        res['number']=d['dealer__user__mobile']
        res['currentAmount']=d['amount']
        res['created_at']=d['created_at'].astimezone(zone).strftime(dt_format)
        res['auto_bid_amount']=d['auto_bid_amount']
        res['bid_start']=d['listing_auction__start_time'].astimezone(zone).strftime(dt_format)
        res['bid_end']=d['listing_auction__end_time'].astimezone(zone).strftime(dt_format)
        response.append(res)    
    context= {
        'response':response,
        }
    return render(request,'closed.html',context)

def closed_city(request,city_name):
    response = []
    path_info = request.META.get('PATH_INFO')
    print(path_info)
    k='scheduled'
    if not k in path_info:
        bids = DealerListingBid.objects.filter(listing_auction__end_time__lt=now,dealer__user__city__name=city_name).values('listing_auction_id').annotate(count=Max('id')).values_list('count',flat=True)
        dealer_listing_data=DealerListingBid.objects.filter(id__in=bids).values('listing_auction_id','listing_auction__listing__id','listing_auction__listing__variant__name','dealer__user__name','dealer__user__mobile','dealer__user__city__name','amount','created_at','listing_auction__start_time','listing_auction__end_time','auto_bid_amount').order_by('-created_at')   
    else:
        bids = DealerListingBid.objects.filter(listing_auction__start_time__gt=now,dealer__user__city__name=city_name).values('listing_auction_id').annotate(count=Max('id')).values_list('count',flat=True)
        dealer_listing_data=DealerListingBid.objects.filter(id__in=bids).values('listing_auction_id','listing_auction__listing__id','listing_auction__listing__variant__name','dealer__user__name','dealer__user__mobile','dealer__user__city__name','amount','created_at','listing_auction__start_time','listing_auction__end_time','auto_bid_amount')    
   
    for d in dealer_listing_data:
        res = {}
        res['Auction_id']=d['listing_auction_id']
        res['CarId']=d['listing_auction__listing__id']
        res['Car']=d['listing_auction__listing__variant__name']
        res['Dealer_name']=d['dealer__user__name']
        res['number']=d['dealer__user__mobile']
        res['currentAmount']=d['amount']
        res['created_at']=d['created_at'].astimezone(zone).strftime(dt_format)
        res['auto_bid_amount']=d['auto_bid_amount']
        res['bid_start']=d['listing_auction__start_time'].astimezone(zone).strftime(dt_format)
        res['bid_end']=d['listing_auction__end_time'].astimezone(zone).strftime(dt_format)
        response.append(res)    
    context= {
        'response':response,
        }
    html = render_to_string('data.html', context)
    return HttpResponse(html)