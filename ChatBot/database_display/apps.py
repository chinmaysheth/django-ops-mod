from django.apps import AppConfig


class DatabaseDisplayConfig(AppConfig):
    name = 'database_display'
