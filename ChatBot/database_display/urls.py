from django.urls import path

from .views import index,dataFromDB,dataFromFB,dataFromDBcity,db,closed_city,closed

app_name = 'database_display'

urlpatterns = [
    # path('db', index, name='index'),
    path('dataFromDB',dataFromDB,name='Data from DB'),
    path('dataFromFB',dataFromFB,name='Data from FB'),
    path('dataFromDB/closed/',closed, name='Closed'),
    path('dataFromDB/closed/<str:city_name>/',closed_city, name='Closed'),
    path('dataFromDB/scheduled/',closed, name='Closed'),
    path('dataFromDB/scheduled/<str:city_name>/',closed_city, name='Closed'),
    path('dataFromDB/<str:city_name>/',dataFromDBcity, name='City'),
    path('dataFromDB/db/',db, name='db'),
   
]