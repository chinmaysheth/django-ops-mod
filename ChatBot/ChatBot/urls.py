from django.conf.urls import include,url
from django.urls import path
from django.contrib import admin


urlpatterns = [
    path('chat/', include('chat.urls',namespace='chat')),
    path('database/', include('database_display.urls',namespace='database')),
    path('admin/', admin.site.urls),
]
